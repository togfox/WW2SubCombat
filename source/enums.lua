enums = {}

function enums.load()
    enum = {}

    enum.sceneMainMenu = 1
    enum.sceneCredits = 2
    enum.sceneNavigator = 3
    enum.sceneSonar = 4

    enum.buttonMainMenuExitGame = 1
    enum.buttonMainMenuContinue = 2

    enum.fontDefault = 1
    enum.fontMedium = 2
    enum.fontLarge = 3
    enum.fontCorporate = 4
    enum.fontalienEncounters48 = 5

    enum.audioMainMenu = 1
    -- add extra items below this line

    enum.objTypeSubmarine = 1           -- ensure these are 1 -> 16 so it works with Box2d
    enum.objTypeTorpedo = 2             -- ensure these line up with enum.image
    enum.objTypeDestroyer = 3
    enum.objTypeBattleship = 4

    enum.audioTorpedoLaunch = 2
    enum.audioShipDestroyed1 = 3
    enum.audioMouseClick = 4

    enum.imageSub = 1           -- ensure these line up with enum.objType
    enum.imageTorpedo = 2
    enum.imageDestroyer = 3
    enum.imageBattleship = 4

    enum.imageButtonOff = 10
    enum.imageLightOff = 11
    enum.imageLightGreen = 12
    enum.imageLightBlue = 13
    enum.imageButtonRedLarge = 14
    enum.imageSheetSmokeFire = 15
    enum.imageSheetDestroyerSinking = 16
    enum.imageButtonSpeedUp = 17
    enum.imageButtonSpeedDown =18
    enum.imageSheetTorpedo = 19
    enum.imageSheetBattleshipSinking = 20

    enum.inputfieldDesiredFacing = 1
    enum.inputfieldTube1Heading = 2         -- don't change this sequence
    enum.inputfieldTube2Heading = 3
    enum.inputfieldTube3Heading = 4
    enum.inputfieldTube4Heading = 5
    enum.inputfieldTube5Heading = 6
    enum.inputfieldDesiredDepth = 7

    -- buttons
    enum.buttonDesiredHeading = 1
    enum.buttonDesiredSpeed0 = 2
    enum.buttonDesiredSpeed25 = 3
    enum.buttonDesiredSpeed50 = 4
    enum.buttonDesiredSpeed75 = 5
    enum.buttonDesiredSpeed100 = 6
    enum.buttonTube1Heading = 7
    enum.buttonTube2Heading = 8
    enum.buttonTube3Heading = 9
    enum.buttonTube4Heading = 10
    enum.buttonTube5Heading = 11
    enum.buttonFireTube1 = 12         -- don't change this sequence
    enum.buttonFireTube2 = 13
    enum.buttonFireTube3 = 14
    enum.buttonFireTube4 = 15
    enum.buttonFireTube5 = 16
    enum.buttonLoadTube1 = 17
    enum.buttonLoadTube2 = 18
    enum.buttonLoadTube3 = 19
    enum.buttonLoadTube4 = 20
    enum.buttonLoadTube5 = 21
    enum.buttonSpeedUp = 22
    enum.buttonSpeedDown = 23
    enum.buttonDesiredDepth = 24
    enum.buttonStopWatch1 = 25          -- don't change this sequence/number
    enum.buttonStopWatch1 = 26
    enum.buttonStopWatch1 = 27

    enum.alertNormal = 1
    enum.alertSearching = 2
    enum.alertEngaging = 3
    enum.alertFleeing = 4
    enum.alertSurrendered = 5

    enum.stopwatchReset = 1
    enum.stopwatchStarted = 2
    enum.stopwatchStopped = 3

    enum.detectedNone = 0        -- ensure this is zero
    enum.detectedVisual = 1
    enum.detectedAudio = 2
    enum.detectedVisualAudio = 3

end

-- create a new text box
-- create new enum enum.inputfield
-- create new enum enum.button
-- add the new inputfield in function functions.initialiseInputFields()
-- add a new button click zone in submarine.loadButtons()
-- check for mouse click in submarine.mousereleased(x, y, button, isTouch)
-- process keystrokes in love.keyreleased(key, scancode) or add code to check new input field and take action

return enums
