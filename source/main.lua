inspect = require 'lib.inspect'
-- https://github.com/kikito/inspect.lua

res = require 'lib.resolution_solution'
-- https://github.com/Vovkiv/resolution_solution

Camera = require 'lib.cam11.cam11'
-- https://notabug.org/pgimeno/cam11

bitser = require 'lib.bitser'
-- https://github.com/gvx/bitser

nativefs = require 'lib.nativefs'
-- https://github.com/EngineerSmith/nativefs

lovelyToasts = require 'lib.lovelyToasts'
-- https://github.com/Loucee/Lovely-Toasts

InputField = require 'lib.InputField'
-- https://github.com/ReFreezed/InputField

S = require 'lib.Strike'
-- https://github.com/Aweptimum/Strike

-- these are core modules
require 'enums'
require 'constants'
fun = require 'functions'
cf = require 'lib.commonfunctions'
button = require 'lib.buttons'
physicslib = require 'physics'

require 'submarine'
require 'torp'
require 'destroyer'
require 'battleship'

require 'navigator'
require 'sonar'

function love.resize(w, h)
	res.resize(w, h)
	-- this force a redraw
	cam:setPos(TRANSLATEX + 1, TRANSLATEY)
end

function beginContact(fixtureA, fixtureB, coll)
	-- a and be are fixtures
	local catA = fixtureA:getCategory()
	local catB = fixtureB:getCategory()
	local guidA = fixtureA:getUserData()
	local guidB = fixtureB:getUserData()
	local objA = fun.getObject(guidA)		-- returns the object
	local objB = fun.getObject(guidB)

	-- print("objA:")
	-- print(inspect(objA))
	-- print("objB:")
	-- print(inspect(objB))

	local addFlame = false		-- boolean
	local x, y, facingrad
	local objwidth, objlength
	if catA == enum.objTypeTorpedo or catB == enum.objTypeTorpedo then
		if catA == enum.objTypeBattleship or catB == enum.objTypeBattleship then
			print("BS dead")
			addFlame = true
		elseif catA == enum.objTypeDestroyer or catB == enum.objTypeDestroyer then
			print("DS dead")
			addFlame = true
		end

		if addFlame then
			if catA == enum.objTypeBattleship or catA == enum.objTypeDestroyer then
				x, y = objB.physobj.body:getPosition()		-- objB is the torp
				facingrad = objA.physobj.body:getAngle()

			elseif catB == enum.objTypeBattleship or catB == enum.objTypeDestroyer then
				x, y = objA.physobj.body:getPosition()		-- objA is the torp
				facingrad = objB.physobj.body:getAngle()
			else
				error()		-- should not happen
			end
			fun.addFlameAnimation(x, y, facingrad)	-- ensure x/y is for the torp (impact point)

			if catA == enum.objTypeDestroyer or catB == enum.objTypeDestroyer then
				-- destroyer sinking
				if catA == enum.objTypeDestroyer then
					x, y = objA.physobj.body:getPosition()
					facingrad = objA.physobj.body:getAngle()
					objwidth = objA.width
					objlength = objA.length
				elseif catB == enum.objTypeDestroyer then
					x, y = objB.physobj.body:getPosition()
					facingrad = objB.physobj.body:getAngle()
					objwidth = objB.width
					objlength = objB.length
				end
				facingrad = facingrad + math.pi / 2			-- 90 degrees
                local newanim = {}
                newanim.imageenum = enum.imageSheetDestroyerSinking        -- only the enum
				newanim.framewidth = FRAMES[newanim.imageenum].width		-- don't change this
				newanim.frameheight = FRAMES[newanim.imageenum].height		-- don't change this
                newanim.startframe = 1
                newanim.stopframe = 12

				newanim.linkobj = nil			-- animation is not linked to an object so does not move with any object (static)
                newanim.x = x   -- destroyer
                newanim.y = y

				newanim.xscale = objwidth / newanim.framewidth
				newanim.yscale = objlength / newanim.frameheight

                newanim.speed = 0.25       -- seconds per frame
                newanim.rotation = facingrad     -- in radians
                newanim.currentframe = newanim.startframe
                newanim.timeleft = newanim.speed                -- how much time left on this frame
                newanim.kill = false
                table.insert(ANIMATIONS, newanim)
			elseif catA == enum.objTypeBattleship or catB == enum.objTypeBattleship then
                -- battleship sinking
				if catA == enum.objTypeBattleship then
					x, y = objA.physobj.body:getPosition()
					facingrad = objA.physobj.body:getAngle()
					objwidth = objA.width
					objlength = objA.length
				elseif catB == enum.objTypeBattleship then
					x, y = objB.physobj.body:getPosition()
					facingrad = objB.physobj.body:getAngle()
					objwidth = objB.width
					objlength = objB.length
				end
				facingrad = facingrad + math.pi / 2			-- 90 degrees
                local newanim = {}
                newanim.imageenum = enum.imageSheetBattleshipSinking        -- only the enum
				newanim.framewidth = FRAMES[newanim.imageenum].width		-- don't change this
				newanim.frameheight = FRAMES[newanim.imageenum].height		-- don't change this
                newanim.startframe = 1
                newanim.stopframe = 9
				newanim.linkobj = nil
                newanim.x = x   -- battleship
                newanim.y = y
				newanim.xscale = objwidth / newanim.framewidth				-- don't change this
				newanim.yscale = objlength / newanim.frameheight			-- don't change this
                newanim.speed = 1       -- seconds per frame
                newanim.rotation = facingrad
                newanim.currentframe = newanim.startframe
                newanim.timeleft = newanim.speed                -- how much time left on this frame
                newanim.kill = false
                table.insert(ANIMATIONS, newanim)
            end
            fun.destroy(objA)
            fun.destroy(objB)
            cf.playAudio(enum.audioShipDestroyed1, false, true)
		end
	end
end

function love.keypressed(key, scancode, isrepeat)
	-- see keydown events in love.update

	for _, inputfield in pairs(INPUTFIELDS) do
		inputfield:keypressed(key, isrepeat)
	end
end

function love.keyreleased(key, scancode )
	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if key == "escape" then
		if currentscene == enum.sceneSonar then
			cf.removeScreen(SCREEN_STACK)
		elseif currentscene == enum.sceneNavigator then
			love.event.quit()
		end
	end

	if key == "space" then		-- pause
		PAUSED = not PAUSED
	end

	if currentscene == enum.sceneNavigator then
		if key == "kp5" then
			-- centre on sub 1
			-- check if InputField has focus
			if FOCUSED_FIELD == nil then
				local objx, objy = SUBMARINE[1].physobj.body:getPosition()
				TRANSLATEX, TRANSLATEY = objx * SCALE, objy * SCALE
			else
				-- don't recentre screen when using an input field
			end
		end
		if key == "f2" then
			cf.swapScreen(enum.sceneSonar, SCREEN_STACK)
		end
		navigator.keyreleased(key, scancode )
	elseif currentscene == enum.sceneSonar then
		if key =="f1" then
			cf.swapScreen(enum.sceneNavigator, SCREEN_STACK)
		end
	else
		-- main menu will error for now
		error()
	end
	if key == "kp+" then
		fun.increaseTime()
	end
	if key == "kp-" then
		fun.decreaseTime()
	end
end

function love.textinput(text)
	for _, inputfield in pairs(INPUTFIELDS) do
		if FOCUSED_FIELD == inputfield then
			inputfield:textinput(text)
		end
	end
end

function love.mousepressed(x, y, button, isTouch)
	local rx, ry = res.toGame(x,y)
	local camx, camy = cam:toWorld(x, y)	-- converts screen x/y to world x/y
	camx = camx / SCALE
	camy = camy / SCALE

	for _, inputfield in pairs(INPUTFIELDS) do
		inputfield:mousepressed(x - inputfield.drawx, y - inputfield.drawy, button, 1)
	end

	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if currentscene == enum.sceneNavigator then
		if button == 1 and ry > TOPBAR_HEIGHT then
			LINE_START_X = camx
			LINE_START_Y = camy
		end
	end
end

function love.mousereleased(x, y, button, isTouch)
	local rx, ry = res.toGame(x,y)
	local camx, camy = cam:toWorld(x, y)	-- converts screen x/y to world x/y
	camx = camx / SCALE
	camy = camy / SCALE

	for _, inputfield in pairs(INPUTFIELDS) do
		inputfield:mousereleased(x - inputfield.drawx, y - inputfield.drawy, button)
	end

	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if currentscene == enum.sceneNavigator then
		navigator.mousereleased(x, y, button, isTouch)		-- processes buttons and inputfields
	elseif currentscene == enum.sceneSonar then
		sonar.mousereleased(x, y, button, isTouch)		-- processes buttons

	end
end

function love.mousemoved( x, y, dx, dy, istouch )
	for _, inputfield in pairs(INPUTFIELDS) do
		inputfield:mousemoved(x - inputfield.drawx, y - inputfield.drawy)
	end

	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if currentscene == enum.sceneNavigator then
		if love.mouse.isDown(2) or love.mouse.isDown(3) then
			local distx, disty = (1 / ZOOMFACTOR) * dx, (1 / ZOOMFACTOR) * dy

	        TRANSLATEX = TRANSLATEX - distx
	        TRANSLATEY = TRANSLATEY - disty
	    end
	end
end

function love.wheelmoved(x, y)
	for _, inputfield in pairs(INPUTFIELDS) do
		inputfield:wheelmoved(x, y)
	end

	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if currentscene == enum.sceneNavigator then
		if y > 0 then
			-- wheel moved up. Zoom in
			ZOOMFACTOR = ZOOMFACTOR + 0.05
		end
		if y < 0 then
			ZOOMFACTOR = ZOOMFACTOR - 0.05
		end
		if ZOOMFACTOR < 0.05 then ZOOMFACTOR = 0.05 end
		if ZOOMFACTOR > 3 then ZOOMFACTOR = 3 end
		print("Zoom factor = " .. ZOOMFACTOR)
	end
end

function love.load()

	local monitor
	if love.filesystem.isFused() then
		monitor = 1
	else
		monitor = 1
	end

	res.init({width = 1920, height = 1080, mode = 2})
	-- local width, height = love.window.getDesktopDimensions(monitor)
	local width, height = 800,600
	res.setMode(width, height, {resizable = true, display = monitor, fullscreen = false, borderless = false})

	constants.load()		-- also loads enums
	fun.loadFonts()
    fun.loadAudio()
	fun.loadImages()

	-- mainmenu.loadButtons()

	if DEBUG then ZOOMFACTOR = 1 end
	cam = Camera.new(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 1)
	cam:setZoom(ZOOMFACTOR)

	love.window.setTitle("Silent Strike " .. GAME_VERSION)

	love.keyboard.setKeyRepeat(true)

	cf.addScreen(enum.sceneMainMenu, SCREEN_STACK)
	cf.addScreen(enum.sceneNavigator, SCREEN_STACK)

	lovelyToasts.canvasSize = {SCREEN_WIDTH, SCREEN_HEIGHT}
	lovelyToasts.options.tapToDismiss = true
	lovelyToasts.options.queueEnabled = true
	-- =============================================

	love.physics.setMeter(1)
	PHYSICSWORLD = love.physics.newWorld(0,0,false)
	PHYSICSWORLD:setCallbacks( beginContact, endContact, preSolve, postSolve )

	submarine.initialise(1)
	navigator.loadButtons()

	destroyer.create()
	destroyer.create()
	destroyer.create()
	destroyer.create()
	battleship.create()

	local subx, suby = SUBMARINE[1].physobj.body:getPosition()
	TRANSLATEX, TRANSLATEY = subx * SCALE, suby * SCALE

	-- set up inputfields
	fun.initialiseInputFields(SUBMARINE[1])
end

function love.draw()
    res.start()

	-- use this to debug scales

	-- love.graphics.rectangle("line", 400, 400, 6, 67)
	-- love.graphics.rectangle("line", 400, 350, 5, 7)


	local currentscene = cf.currentScreenName(SCREEN_STACK)
	if currentscene == enum.sceneNavigator then
		navigator.draw()
	elseif currentscene == enum.sceneSonar then
		sonar.draw()
	else
		error()
	end



	buttons.drawButtons()
	lovelyToasts.draw()
    res.stop()
end

function love.update(dt)

	if not PAUSED then
		submarine.update(dt)
		torp.update(dt)
		destroyer.update(dt)
		battleship.update(dt)

		fun.animationsupdate(dt)

		local currentscene = cf.currentScreenName(SCREEN_STACK)
		if currentscene == enum.sceneNavigator then
			navigator.update(dt)
			fun.updateWorldClock(dt)
		elseif currentscene == enum.sceneSonar then
			sonar.update(dt)
		else
			print("currentscene is " .. currentscene)
			if currentscene == enum.sceneMainMenu then
				cf.addScreen(enum.sceneNavigator, SCREEN_STACK)
			else
				error()
			end
		end

		PHYSICSWORLD:update(dt * TIME) --this puts the world into motion
	end

	cam:setZoom(ZOOMFACTOR)
    cam:setPos(TRANSLATEX, TRANSLATEY)

	if TIMER_SUB_DEAD > 0 then TIMER_SUB_DEAD = TIMER_SUB_DEAD - dt end

	lovelyToasts.update(dt)
end
