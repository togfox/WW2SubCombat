constants = {}

function constants.load()

    GAME_VERSION = "0.06"

    SCREEN_STACK = {}

    -- SCREEN_WIDTH, SCREEN_HEIGHT = love.window.getDesktopDimensions(1)
    SCREEN_WIDTH, SCREEN_HEIGHT = res.getGame()

    -- camera
    ZOOMFACTOR = 0.2
    TRANSLATEX = cf.round(SCREEN_WIDTH / 2)		-- starts the camera in the middle of the ocean
    TRANSLATEY = cf.round(SCREEN_HEIGHT / 2)	-- need to round because this is working with pixels

    cam = nil       -- camera
    AUDIO = {}
    MUSIC_TOGGLE = true
    SOUND_TOGGLE = true

    IMAGE = {}
    FONT = {}

    -- set the folders based on fused or not fused
    savedir = love.filesystem.getSourceBaseDirectory()
    if love.filesystem.isFused() then
        savedir = savedir .. "\\savedata\\"
    else
        savedir = savedir .. "/SilentStrike/savedata/"
    end

    enums.load()

    -- add extra items below this line **************************************************************
    PAUSED = false
    DEBUG = true
    -- DEBUG = false
    if love.filesystem.isFused() then DEBUG = false end

    SUBMARINE = {}
    TORPEDO = {}
    DESTROYER = {}
    BATTLESHIP = {}

    INPUTFIELDS = {}
    ANIMATIONS = {}
    FRAMES = {}
    LINES = {}
    STOPWATCH = {}          -- multiple stop watches
    STOPWATCH[1] = {}
    STOPWATCH[1].value = 0
    STOPWATCH[1].mode = enum.stopwatchReset
    SONAR_MARKS = {}                                  -- player placed marks for suspected sonar targets

    SCALE = 1

    LABEL_MARGIN = 5    -- the gap between label and input field (pixels)
    SECONDS_TO_LOAD_TUBE = 1200
    SECONDS_TO_LOAD_TUBE = 45           -- use this for play testing
    TORPEDO_RUN_TIME = 500              -- seconds

    TIME = 1                            -- time acceleration
    DEPTH_CHANGE = 0.05                    -- metres per second to rise/dive
    LINE_TIME = 180                          -- number of seconds it takes for a nav line to fade

    CURRENT_DIRECTION = love.math.random(0, 359)
    CURRENT_FORCE = love.math.random(0, 100)
    -- CURRENT_FORCE = 0

    FOCUSED_FIELD = nil         -- the inputfield that has focus

    -- drawing things
    TOPBAR_HEIGHT = 100
    LINE_START_X = nil          -- tracks the drawing of lines on the nav station
    LINE_START_Y = nil

    TIMER_SUB_DEAD = 0          -- used to moderate the frequency of lovelyToasts

    NEXT_DETECTION_CHECK_MASTER = 5     -- seconds. When to do the next check for detection
    SECOND = 0            -- game seconds passed/age of game/career

    -- sonar constants for sonar scenes
    STATIC = {}
    STATIC_DENSITY = 7000
    STATIC_TIMER_MASTER = 5    -- the value to use when resetting this value
    STATIC_TIMER = 0            -- seconds
    STATICWIDTH = 5
    STATICHEIGHT = 5
    SCOPE_RADIUS = 350
    SCOPE_RANGE = 2000          -- the scope won't display anything beyond 2000 metres

    GAME_YEAR = 1942
    GAME_MONTH = 1
    GAME_DATE = 1
    GAME_HOUR = 6           -- 24 hour time
    GAME_MINUTE = 0
    GAME_SECOND = 0


end

return constants
