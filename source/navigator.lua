navigator = {}

function navigator.drawTopbar()
    love.graphics.setColor(0,0,0,1)
    love.graphics.rectangle("fill", 0, 0, SCREEN_WIDTH, TOPBAR_HEIGHT)
    love.graphics.setColor(1,1,1,1)
    -- speed buttons
    love.graphics.print("Speed setting", 35, 15)

    -- torpedo heading
    love.graphics.setColor(1,1,1,1)
    love.graphics.print("Heading", 1087, 3)

    love.graphics.print("Time to load", 1190, 0)

    -- draw all the input fields
    for _, inputfield in pairs(INPUTFIELDS) do
        -- need to print the label first and then the inputfield after that.
        -- get the length of the label
        local font = love.graphics.getFont()
        local textobj = love.graphics.newText(font, inputfield.label)
        local fieldlength, fieldheight = textobj:getDimensions()

        love.graphics.setColor(1,1,1,1)
        love.graphics.print(inputfield.label, inputfield.drawx, inputfield.drawy)

        -- print the highlight - if any
        local fieldX = inputfield.drawx + fieldlength + LABEL_MARGIN
        local fieldY = inputfield.drawy
        love.graphics.setColor(0, 0, 1)
        for _, x, y, w, h in inputfield:eachSelection() do
            love.graphics.rectangle("fill", fieldX+x, fieldY+y, w, h)
        end
        love.graphics.setColor(1, 1, 1)
        for _, text, x, y in inputfield:eachVisibleLine() do
            love.graphics.print(text, fieldX+x, fieldY+y)
        end

        -- this draws the cursor
        if FOCUSED_FIELD == inputfield then
            local x, y, h = inputfield:getCursorLayout()
            love.graphics.rectangle("fill", fieldX+x, fieldY+y, 1, h)
        end
    end

    -- draw the current facing in the top bar
    love.graphics.setColor(1,1,1,1)
    love.graphics.print("Current facing:", 405, 20)

    local currentfacing = cf.round(physicslib.getCompassHeading(SUBMARINE[1]), 0)
    if currentfacing == 360 then currentfacing = 0 end

    love.graphics.print(cf.round(currentfacing, 0), 500, 20)

    love.graphics.setColor(1,1,1,1)
    love.graphics.print("Current speed:",850, 20)
    love.graphics.print(cf.round(SUBMARINE[1].speed, 1), 950, 20)
    love.graphics.print("m/s", 975, 20)

    love.graphics.setColor(1,1,1,1)
    love.graphics.print("Current depth:", 550, 20)
    love.graphics.print(cf.round(SUBMARINE[1].depth, 1), 643, 20)
    love.graphics.print("metres", 675, 20)


    -- draw torpedo tube lights. The input field is done elsewhere
    -- the fire button is a button object with it's own image
    local panelx = 1145       -- make these relative to top left corner x/y for easy moving the whole panel
    local panely = 20
    local rowheight = 15
    for i = 1, #SUBMARINE[1].tube do
        tube = SUBMARINE[1].tube[i]

        love.graphics.setColor(1,1,1,1)
        if tube.timetoload == nil then          -- tube is empty
            love.graphics.draw(IMAGE[enum.imageLightOff], panelx, panely, 0, 1, 1, 0, 0)
        elseif tube.timetoload > 0 then       -- loading or queued to laod
            love.graphics.draw(IMAGE[enum.imageLightBlue], panelx, panely, 0, 1, 1, 0, 0)
        elseif tube.timetoload == 0 then
            love.graphics.draw(IMAGE[enum.imageLightGreen], panelx, panely, 0, 1, 1, 0, 0)
        else
            print(tube.timetoload)
            error()
        end

        -- draw timers for tubes
        love.graphics.setColor(1,1,1,1)

        if tube.timetoload ~= nil and tube.timetoload ~= 0 then
            local hours, mins, secs = cf.time_split(cf.round(tube.timetoload, 0))
            local txt = mins .. ":" .. secs
            love.graphics.print(txt, 1205, panely + ((i - 1)))
        end
        panely = panely + rowheight
    end

    -- time acceleration
    love.graphics.setColor(1,1,1,1)
    local txt = ""
    if PAUSED then
        txt = "PAUSED"
    else
        txt = "Time x" .. TIME
    end
    love.graphics.print(txt, 1350, 5)

    -- stop watches
    local drawx = 1600
    local drawy = 5
    local txt = cf.round(STOPWATCH[1].value) .. " seconds"
    love.graphics.print("Stop watch", drawx, drawy)
    love.graphics.print(txt, drawx + 65, drawy + 20)   -- needs to be a loop for each stop watch

    -- draw world clock
    local drawx = 1800
    local draw = 10
    love.graphics.print("Clock", drawx, drawy)
    local txt = string.format("%02d:%02d:%02d",GAME_HOUR,GAME_MINUTE,cf.round(GAME_SECOND))         -- round the seconds during display - not during update
    love.graphics.print(txt, drawx, drawy + 20)
    txt = GAME_DATE .. "/" .. GAME_MONTH .. "/" .. GAME_YEAR
    love.graphics.print(txt, drawx, drawy + 40)
end

function navigator.drawSideBar()

    -- draw black box
    local sidebarwidth = 150
    love.graphics.setColor(1,1,1,1)
    love.graphics.rectangle("fill", SCREEN_WIDTH - sidebarwidth, 0, sidebarwidth, SCREEN_HEIGHT)

    -- draw water
    love.graphics.setColor(97/255,73/255,1,1)
    love.graphics.rectangle("fill", SCREEN_WIDTH - sidebarwidth + 10, TOPBAR_HEIGHT, sidebarwidth - 10, SCREEN_HEIGHT - 10)
end

local function drawLines()
    -- draw the 'in progress' line

    local fontsize
    local font
    fontsize = 15 / ZOOMFACTOR            -- ensure the font is viewable at all zoom values
    if fontsize < 15 then fontsize = 15 end
    font = love.graphics.newFont("assets/fonts/Vera.ttf", fontsize)
    love.graphics.setFont(font)

    if love.mouse.isDown(1) then
        local mousex = love.mouse.getX()
        local mousey = love.mouse.getY()
        local rx, ry = res.toGame(mousex, mousey)
        if ry > TOPBAR_HEIGHT and LINE_START_X ~= nil then
            -- draw line
            love.graphics.setColor(0,1,0,1)
            local camx, camy = cam:toWorld(mousex, mousey)	-- converts screen x/y to world x/y
            camx = camx / SCALE
            camy = camy / SCALE

            love.graphics.line(LINE_START_X, LINE_START_Y, camx, camy)

            -- draw metrics
            local linelength = cf.getDistance(LINE_START_X, LINE_START_Y, camx, camy)
    		local lineheading = cf.getBearing(LINE_START_X, LINE_START_Y, camx, camy)
            local dist = linelength / 2
            local txt = cf.round(linelength) .. " metres" .. "\n" .. lineheading .. " degrees"
            local drawx, drawy = cf.addVectorToPoint(LINE_START_X, LINE_START_Y, lineheading, dist)
            love.graphics.print(txt, drawx,drawy)
        end
    end

    -- draw the established lines
    for k, line in pairs(LINES) do
        love.graphics.setColor(0,1,0,line.timeleft / LINE_TIME)
        if line.length == 0 then
            -- line is actually a point
            love.graphics.circle("fill", line.startx, line.starty, 5)

        else
            -- line is not a point
            love.graphics.line(line.startx, line.starty, line.stopx, line.stopy)
            -- draw metrics
            -- get the "halfway" point to draw things
            local dist = line.length / 2
            local drawx, drawy = cf.addVectorToPoint(line.startx, line.starty, line.heading, dist)

            local txt = cf.round(line.length) .. " metres" .. "\n" .. line.heading .. " degrees"

            love.graphics.print(txt, drawx,drawy)
        end
    end
    love.graphics.setFont(FONT[enum.fontDefault])
    love.graphics.setColor(1,1,1,1)
end

local function drawMarks()
    -- draw sonar marks
    local subx, suby = SUBMARINE[1].physobj.body:getPosition()
    for k, mark in pairs(SONAR_MARKS) do
        love.graphics.setColor(1,0,0, mark.timeleft / LINE_TIME)
        love.graphics.circle("line", mark.x, mark.y, 5)
    end

end


local function updateLines(dt)
    for i = #LINES, 1, -1 do
        LINES[i].timeleft = LINES[i].timeleft - dt            -- fade the line over time
        if LINES[i].timeleft <= 0 then table.remove(LINES, i) end
    end
end

function navigator.deleteLastLine()
    -- delete the youngest/newest line. Usually when control-z is pressed
    if #LINES > 0 then
        table.remove(LINES, #LINES)
    end
end

local function clickReloadButton(tube, clickedButtonID)
    -- this is a toggle button that does different things
    -- tube is the tube number (number)
    -- clickedButtonID is the button enum that was clicked

    cf.playAudio(enum.audioMouseClick, false, true)

    if SUBMARINE[1].tube[tube].timetoload == nil then
        SUBMARINE[1].tube[tube].timetoload = SECONDS_TO_LOAD_TUBE
        buttons.changeButtonLabel(clickedButtonID, "Cancel")
    elseif SUBMARINE[1].tube[tube].timetoload == 0 then
        -- tube loaded. Do nothing
    elseif SUBMARINE[1].tube[tube].timetoload > 0 then
        -- loading in progress. Cancel it.
        SUBMARINE[1].tube[tube].timetoload = nil
        buttons.changeButtonLabel(clickedButtonID, "Reload")
    end
end

local function createNewLine(mousex, mousey)
    -- called when mouse button is released. Adds a new line to the LINES table
    -- mouse x/y is where the mouse was released. Send in cam11 values AFTER scaling

    local newline = {}
    newline.startx = LINE_START_X
    newline.starty = LINE_START_Y
    newline.stopx = mousex
    newline.stopy = mousey
    newline.timeleft = LINE_TIME                        -- this will fade over time
    newline.length = cf.getDistance(newline.startx, newline.starty, newline.stopx, newline.stopy)

    newline.heading = cf.getBearing(newline.startx, newline.starty, newline.stopx, newline.stopy)
    table.insert(LINES, newline)
end

local function updateStopwatches(dt)
    -- update stop watches if mode == started
    for i = 1, #STOPWATCH do
        if STOPWATCH[i].mode == enum.stopwatchStarted then
            STOPWATCH[i].value = STOPWATCH[i].value + dt
        end
    end
end

function navigator.keyreleased(key, scancode )
    if FOCUSED_FIELD == INPUTFIELDS[enum.inputfieldDesiredFacing] then
        local df = tonumber(INPUTFIELDS[enum.inputfieldDesiredFacing]:getText())
        if df ~= nil and df >= 0 and df <= 359 then
            SUBMARINE[1].desiredfacing = df
            -- print("DF is now " .. df)
        else
            SUBMARINE[1].desiredfacing = physicslib.getCompassHeading(SUBMARINE[1])		-- stop turning if input field doesn't make sense
        end
    end

    if FOCUSED_FIELD == INPUTFIELDS[enum.inputfieldDesiredDepth] then
        local dd = tonumber(INPUTFIELDS[enum.inputfieldDesiredDepth]:getText())
        if dd ~= nil and dd >=0 and dd <= 250 then
            SUBMARINE[1].desireddepth = dd
        else
            SUBMARINE[1].desireddepth = SUBMARINE[1].depth		-- stop turning if input field doesn't make sense
        end
    end

    if DEBUG and key == "f12" then
        BATTLESHIP[1].desiredfacing = 180
    end

    if key == "z" and (love.keyboard.isDown("lctrl") or love.keyboard.isDown("rctrl")) then
        -- undo last line drawn
        navigator.deleteLastLine()
    end
end

function navigator.mousereleased(x, y, button, isTouch)
    local rx, ry = res.toGame(x,y)
    local camx, camy = cam:toWorld(x, y)	-- converts screen x/y to world x/y


    if button == 1 then
        FOCUSED_FIELD = nil     -- clear it here and then set it below

        local clickedButtonID = buttons.getButtonID(rx, ry)

        if clickedButtonID ~= nil then print("Button ID clicked: " .. clickedButtonID) end

        if clickedButtonID == enum.buttonDesiredHeading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldDesiredFacing]
            INPUTFIELDS[enum.inputfieldDesiredFacing]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonDesiredDepth then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldDesiredDepth]
            INPUTFIELDS[enum.inputfieldDesiredDepth]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)

        elseif clickedButtonID == enum.buttonTube1Heading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldTube1Heading]
            INPUTFIELDS[enum.inputfieldTube1Heading]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonTube2Heading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldTube2Heading]
            INPUTFIELDS[enum.inputfieldTube2Heading]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonTube3Heading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldTube3Heading]
            INPUTFIELDS[enum.inputfieldTube3Heading]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonTube4Heading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldTube4Heading]
            INPUTFIELDS[enum.inputfieldTube4Heading]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonTube5Heading then
            FOCUSED_FIELD = INPUTFIELDS[enum.inputfieldTube5Heading]
            INPUTFIELDS[enum.inputfieldTube5Heading]:selectAll()
            cf.playAudio(enum.audioMouseClick, false, true)

        elseif clickedButtonID == enum.buttonDesiredSpeed0 then
            SUBMARINE[1].currentforce = 0
            SUBMARINE[1].enginesetting = 0      -- 0%
            print("Stopping engines")
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonDesiredSpeed25 then
            SUBMARINE[1].currentforce = SUBMARINE[1].maxforce * 0.25
            SUBMARINE[1].enginesetting = 0.25               -- 25%
            print("Setting engines to 25%")
            print(SUBMARINE[1].currentforce)
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonDesiredSpeed50 then
            SUBMARINE[1].currentforce = SUBMARINE[1].maxforce * 0.50
            SUBMARINE[1].enginesetting = 0.50               -- 50%
            print("Setting engines to 50%")
            print(SUBMARINE[1].currentforce)
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonDesiredSpeed75 then
            SUBMARINE[1].currentforce = SUBMARINE[1].maxforce * 0.75
            SUBMARINE[1].enginesetting = 0.75
            print("Setting engines to 75%")
            print(SUBMARINE[1].currentforce)
            cf.playAudio(enum.audioMouseClick, false, true)
        elseif clickedButtonID == enum.buttonDesiredSpeed100 then
            SUBMARINE[1].currentforce = SUBMARINE[1].maxforce
            SUBMARINE[1].enginesetting = 1                  -- 100%
            print("Setting engines to 100%")
            print(SUBMARINE[1].currentforce)
            cf.playAudio(enum.audioMouseClick, false, true)

        elseif clickedButtonID == enum.buttonFireTube1 then
            submarine.tryToFireTorpedo(1)     -- tube number
        elseif clickedButtonID == enum.buttonFireTube2 then
            submarine.tryToFireTorpedo(2)     -- tube number
        elseif clickedButtonID == enum.buttonFireTube3 then
            submarine.tryToFireTorpedo(3)     -- tube number
        elseif clickedButtonID == enum.buttonFireTube4 then
            submarine.tryToFireTorpedo(4)     -- tube number
        elseif clickedButtonID == enum.buttonFireTube5 then
            submarine.tryToFireTorpedo(5)     -- tube number

        elseif clickedButtonID == enum.buttonLoadTube1 then     -- is a toggle
            clickReloadButton(1, clickedButtonID)
        elseif clickedButtonID == enum.buttonLoadTube2 then     -- is a toggle
            clickReloadButton(2, clickedButtonID)
        elseif clickedButtonID == enum.buttonLoadTube3 then     -- is a toggle
            clickReloadButton(3, clickedButtonID)
        elseif clickedButtonID == enum.buttonLoadTube4 then     -- is a toggle
            clickReloadButton(4, clickedButtonID)
        elseif clickedButtonID == enum.buttonLoadTube5 then     -- is a toggle
            clickReloadButton(5, clickedButtonID)

        elseif clickedButtonID == enum.buttonSpeedUp then
            fun.increaseTime()
        elseif clickedButtonID == enum.buttonSpeedDown then
            fun.decreaseTime()

        elseif clickedButtonID == enum.buttonStopWatch1 then            -- do other SW here
            fun.adjustStopWatch(1, clickedButtonID)




        else
            -- don't put error trap here
        end
    end

    if ry > TOPBAR_HEIGHT and LINE_START_X ~= nil then
        createNewLine(camx / SCALE, camy / SCALE)
    end
    LINE_START_X = nil
    LINE_START_Y = nil
end

function navigator.draw()

    cam:attach()

    love.graphics.scale(SCALE, SCALE)
    if DEBUG then cf.printAllPhysicsObjects(PHYSICSWORLD, SCALE) end
    fun.animationsdraw()

    love.graphics.setBackgroundColor( 2/255, 5/255, 71/255, 1 )
    for k, sub in pairs(SUBMARINE) do
        submarine.draw(sub)
    end
    for k, tp in pairs(TORPEDO) do
        torp.draw(tp)
    end
    for k, dest in pairs(DESTROYER) do
        destroyer.draw(dest)
    end
    for k, bs in pairs(BATTLESHIP) do
        battleship.draw(bs)
    end
    drawLines()
    drawMarks()                     -- sonar marks
    love.graphics.scale(1, 1)
    cam:detach()

    -- navigator.drawSideBar()
    navigator.drawTopbar()		-- just a grey box
end

function navigator.update(dt)
    updateLines(dt)     -- do not * TIME
    updateStopwatches(dt * TIME)
end

function navigator.loadButtons()

    -- desired heading
    local mybutton = {}
    mybutton.x = 497
    mybutton.y = 47
    mybutton.width = 34                -- use this to define click zone on images
    mybutton.height = 20
    mybutton.bgcolour = {1,1,1,0}       -- set alpha to zero if drawing an image
    mybutton.drawOutline = true
    mybutton.outlineColour = {1,1,1,1}
    mybutton.label = ""
    mybutton.image = nil
    mybutton.imageoffsetx = 0
    mybutton.imageoffsety = 0
    mybutton.imagescalex = 0.5
    mybutton.imagescaley = 0.5
    mybutton.labelcolour = {1,1,1,1}
    mybutton.labeloffcolour = {1,1,1,1}
    mybutton.labeloncolour = {1,1,1,1}
    mybutton.labelcolour = {1,1,1,1}
    mybutton.labelxoffset = 15
    mybutton.state = "on"
    mybutton.visible = true
    mybutton.identifier = enum.buttonDesiredHeading     -- change and add to enum
    mybutton.scene = enum.sceneNavigator
    table.insert(GUI_BUTTONS, mybutton) -- this adds the button to the global table

    -- desired depth
    local mybutton = buttons.create(642, 48, enum.buttonDesiredDepth)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 33)
    buttons.setHeight(mybutton, 20)
    buttons.setBGColour(mybutton, {0,0,0,0})
    buttons.setDrawOutline(mybutton, true)
    buttons.setLabel(mybutton, "")
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- tube heading click zones
    local enumvalue = 7          -- enu.lua has these listed as 2, 3, 4, 5, 6 so this is exploited here
    local drawy = 20
    local rowheight = 15
    for i = 1, 5 do
        local mybutton = buttons.create(1110, drawy, enumvalue)
        buttons.setWidth(mybutton, 30)
        buttons.setHeight(mybutton, 15)
        buttons.setBGColour(mybutton, {1,1,1,0})
        buttons.setVisible(mybutton, true)           -- an invisible button can't be clicked
        buttons.setDrawOutline(mybutton, true)
        buttons.setLabel(mybutton, "")
        buttons.setScene(mybutton, enum.sceneNavigator)
        enumvalue = enumvalue + 1
        drawy = drawy + rowheight
    end

    -- all stop
    local drawx = 25
    local drawy = 40
    local mybutton = buttons.create(drawx, drawy, enum.buttonDesiredSpeed0)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 50)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
    buttons.setScaleX(mybutton, 1.20)
    buttons.setScaleY(mybutton, 1.20)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "STOP")
    buttons.setLabelOffsetX(mybutton, 8)
    buttons.setLabelOffsetY(mybutton, 15)
    buttons.setLabelColour(mybutton, {0,0,0,1} )
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- 25% engines
    local mybutton = buttons.create(drawx + 50, drawy, enum.buttonDesiredSpeed25)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 50)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
    buttons.setScaleX(mybutton, 1.20)
    buttons.setScaleY(mybutton, 1.20)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "25%")
    buttons.setLabelOffsetX(mybutton, 8)
    buttons.setLabelOffsetY(mybutton, 15)
    buttons.setLabelColour(mybutton, {0,0,0,1} )
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- 50% engines
    local mybutton = buttons.create(drawx + 100, drawy, enum.buttonDesiredSpeed50)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 50)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
    buttons.setScaleX(mybutton, 1.20)
    buttons.setScaleY(mybutton, 1.20)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "50%")
    buttons.setLabelOffsetX(mybutton, 8)
    buttons.setLabelOffsetY(mybutton, 15)
    buttons.setLabelColour(mybutton, {0,0,0,1} )
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- 75% engines
    local mybutton = buttons.create(drawx + 150, drawy, enum.buttonDesiredSpeed75)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 50)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
    buttons.setScaleX(mybutton, 1.20)
    buttons.setScaleY(mybutton, 1.20)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "75%")
    buttons.setLabelOffsetX(mybutton, 8)
    buttons.setLabelOffsetY(mybutton, 15)
    buttons.setLabelColour(mybutton, {0,0,0,1} )
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- 100% engines - full flank
    local mybutton = button.create(drawx + 200, drawy, enum.buttonDesiredSpeed100)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 50)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
    buttons.setScaleX(mybutton, 1.20)
    buttons.setScaleY(mybutton, 1.20)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "100%")
    buttons.setLabelOffsetX(mybutton, 8)
    buttons.setLabelOffsetY(mybutton, 15)
    buttons.setLabelColour(mybutton, {0,0,0,1} )
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- tubes
    local drawy = 20
    local rowheight = 15
    local enumvalue = 12
    for i = 1, 5 do
                -- fire button
        local mybutton = buttons.create(1170, drawy, enumvalue)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
        buttons.setLabelColour(mybutton, {0,0,0,1} )
        buttons.setWidth(mybutton, 15)
        buttons.setHeight(mybutton, 15)
        buttons.setLabel(mybutton, "")
        buttons.setImage(mybutton, IMAGE[enum.imageButtonRedLarge])
        buttons.setScaleX(mybutton, 0.25)
        buttons.setScaleY(mybutton, 0.25)
        buttons.setDrawOutline(mybutton, false)
        buttons.setScene(mybutton, enum.sceneNavigator)

        -- reload button
        local mybutton = {}
        mybutton = button.create(1250, drawy, enumvalue + 5)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
        buttons.setLabelColour(mybutton, {0,0,0,1} )
        buttons.setWidth(mybutton, 55)
        buttons.setHeight(mybutton, 15)
        buttons.setLabel(mybutton, "Reload")
        buttons.setLabelOffsetX(mybutton, 8)
        buttons.setLabelOffsetY(mybutton, 15)
        buttons.setLabelOffsetY(mybutton, 0)
        buttons.setImage(mybutton, IMAGE[enum.imageButtonOff])
        buttons.setScaleX(mybutton, 1.25)
        buttons.setScaleY(mybutton, 0.33)
        buttons.setDrawOutline(mybutton, false)
        buttons.setScene(mybutton, enum.sceneNavigator)

        drawy = drawy + rowheight
        enumvalue = enumvalue + 1
    end

    -- time acceleration
    local mybutton = buttons.create(1365, 30, enum.buttonSpeedUp)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 20)
    buttons.setHeight(mybutton, 20)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonSpeedUp])
    buttons.setScaleX(mybutton, 0.5)
    buttons.setScaleY(mybutton, 0.5)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, true)
    buttons.setLabel(mybutton, "")
    buttons.setScene(mybutton, enum.sceneNavigator)

    local mybutton = buttons.create(1365, 60, enum.buttonSpeedDown)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
    buttons.setWidth(mybutton, 20)
    buttons.setHeight(mybutton, 20)
    buttons.setImage(mybutton, IMAGE[enum.imageButtonSpeedDown])
    buttons.setScaleX(mybutton, 0.5)
    buttons.setScaleY(mybutton, 0.5)
    buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
    buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right
    buttons.setDrawOutline(mybutton, false)
    buttons.setLabel(mybutton, "")
    buttons.setScene(mybutton, enum.sceneNavigator)

    -- stop watches
    local drawx = 1600
    local drawy = 25
    local enumvalue = enum.buttonStopWatch1
    for i = 1, #STOPWATCH do
        local mybutton = buttons.create(drawx, drawy, enumvalue)        -- x, y, id. Creates and returns a default button item. Adds it to the global array
        buttons.setWidth(mybutton, 40)
        buttons.setHeight(mybutton, 20)
        buttons.setImage(mybutton,  IMAGE[enum.imageButtonOff])
        buttons.setScaleX(mybutton, 1)
        buttons.setScaleY(mybutton, 0.5)
        buttons.setImageOffsetX(mybutton, 1)     -- moves the image left
        buttons.setImageOffsetY(mybutton, 2)     -- moves teh image right

        if STOPWATCH[i].mode == enum.stopwatchReset then
            buttons.setLabel(mybutton, "Start")
        elseif STOPWATCH[i].mode == enum.stopwatchStarted then
            buttons.setLabel(mybutton, "Stop")
        elseif STOPWATCH[i].mode == enum.stopwatchStopped then
            buttons.setLabel(mybutton, "Reset")
        else
            error()
        end
        buttons.setLabelOffsetX(mybutton, 2)
        buttons.setLabelOffsetY(mybutton, 2)
        buttons.setLabelColour(mybutton, {0,0,0,1} )
        buttons.setDrawOutline(mybutton, false)
        buttons.setScene(mybutton, enum.sceneNavigator)

        enumvalue =enumvalue + 1
        drawy = drawy + 15
    end
end




return navigator
