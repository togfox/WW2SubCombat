submarine = {}

function submarine.initialise(index)
    -- index is normally 1 but might be used in multiplayer or if there are bots

    -- https://en.wikipedia.org/wiki/Type_VII_submarine
    -- top speed = 9.1 m/s surfaced
    -- top speed = 3.9 m/s submerged
    -- turning radius (avg) = 115 metres
    -- turning diametre (avg) = 230 metres
    -- turning time (180 deg) = 70 seconds

    SUBMARINE[index] = {}
    SUBMARINE[index].objType = enum.objTypeSubmarine

    SUBMARINE[index].desiredfacing = 0            -- compass heading
    -- SUBMARINE[index].desiredfacing = 90            -- compass heading

    SUBMARINE[index].length = 67
    SUBMARINE[index].width = 6
    if DEBUG then
        SUBMARINE[index].depth = 12
    else
        SUBMARINE[index].depth = 12
    end
    SUBMARINE[index].desireddepth = SUBMARINE[index].depth
    SUBMARINE[index].fuel = 0
    SUBMARINE[index].mass = 871000  -- 871 tonnes
    SUBMARINE[index].speed = 0                  -- straight-line speed in m/s based on x/y velocity
    SUBMARINE[index].maxunderwaterforce = 775   -- don't use this attribute as its not common to all objects
    SUBMARINE[index].maxsurfaceforce = 1850
    SUBMARINE[index].maxforce = SUBMARINE[index].maxunderwaterforce          -- use this to regulate top speed  3.9 / 9.1
    SUBMARINE[index].currentforce = 0               -- current forward force created by the engines
    SUBMARINE[index].enginesetting = 0                  -- % of engine speed eg 0.25  (25%). Used when surfacing and submerging
    SUBMARINE[index].turnrate = 180/70
    SUBMARINE[index].maxdepth = 230                     -- metres
    SUBMARINE[index].torpstock = 9                    -- this is in storage. There might be more in the tubes. Another 4 in bow tubes and 1 in stern tube
    SUBMARINE[index].noiseradius = 50                   -- noise profile
    SUBMARINE[index].visualradius = 50                   -- visibility profile
    SUBMARINE[index].imageenum = SUBMARINE[index].objType

    -- torpedo tubes (not torpedoes)
    -- https://en.wikipedia.org/wiki/G7e_torpedo  (G7e(TIII)
    -- https://en.wikipedia.org/wiki/File:FiringGeometry.png
    -- https://en.wikipedia.org/wiki/Torpedo_Data_Computer
    SUBMARINE[index].tube = {}
    for i = 1, 5 do
        SUBMARINE[index].tube[i] = {}
        SUBMARINE[index].tube[i].timetoload = 0           -- 0 = it is ready to fire. nil if empty
    end

    -- create a physical object
    local thisobject = {}
    local x = SCREEN_WIDTH  / 2        -- this is the centre - not the corner
    local y = SCREEN_HEIGHT / 2        -- this is the centre - not the corner
    thisobject.body = love.physics.newBody(PHYSICSWORLD, x, y, "dynamic")
    thisobject.body:setLinearDamping(0.5)
    thisobject.body:setMass(SUBMARINE[index].mass)
    thisobject.body:setAngle(math.rad(270))

    -- define the shape of the physical object
    local x1 = SUBMARINE[index].length / 2 * -1
    local y1 = SUBMARINE[index].width / 2 * -1
    local x2 = SUBMARINE[index].length / 2
    local y2 = SUBMARINE[index].width / 2 * -1
    local x3 = SUBMARINE[index].length / 2
    local y3 = SUBMARINE[index].width / 2
    local x4 = SUBMARINE[index].length / 2 * -1
    local y4 = SUBMARINE[index].width / 2
    thisobject.shape = love.physics.newPolygonShape(x2,y2,x3,y3,x4,y4,x1,y1)
    thisobject.fixture = love.physics.newFixture(thisobject.body, thisobject.shape, 1)		-- the 1 is the density
    thisobject.fixture:setRestitution(0.25)                                                 -- bounce factor
    thisobject.fixture:setSensor(false)                                                     -- sensors don't react physically so set this to false
    thisobject.fixture:setCategory(enum.objTypeSubmarine)
    -- thisobject.fixture:setMask(enum.categoryFriendlyFighter, enum.categoryFriendlyBullet, enum.categoryEnemyFighter)     -- this is what it WON'T collide with
    local guid = cf.getGUID()
    thisobject.fixture:setUserData(guid)
    SUBMARINE[index].guid = guid

    SUBMARINE[index].physobj = thisobject
end

function submarine.loadtubes(dt)
    -- decrease the "load" timer for the tube that is being loaded (only one at a time)
    for i = 1, #SUBMARINE[1].tube do
        if SUBMARINE[1].tube[i].timetoload == 0 or SUBMARINE[1].tube[i].timetoload == nil then
            -- do nothing
        else
            SUBMARINE[1].tube[i].timetoload = SUBMARINE[1].tube[i].timetoload - dt
            if SUBMARINE[1].tube[i].timetoload <= 0 then
                SUBMARINE[1].tube[i].timetoload = 0
                buttons.changeButtonLabel(16 + i, "Reload")
            end
            return -- only load one tube at a time
        end
    end
end

function submarine.tryToFireTorpedo(tube)
    print("Firing tube #" .. tube)
    if SUBMARINE[1].tube[tube].timetoload == 0 then
        local newtorp = torp.create(SUBMARINE[1], tube)
        SUBMARINE[1].tube[tube].timetoload = nil
        cf.playAudio(enum.audioTorpedoLaunch, false, true)

        local newtorpfacing = physicslib.getCompassHeading(newtorp)     -- compass degrees

        -- animations
        local newanim = {}
        newanim.imageenum = enum.imageSheetTorpedo        -- only the enum
        newanim.startframe = 1
        newanim.stopframe = 3
        newanim.endless = true                             -- cycle endlessly
        local x, y = newtorp.physobj.body:getPosition()
        newanim.x = x
        newanim.y = y
        newanim.framewidth = 4                                          -- this hardcoding is an exception due to animation being wider than the torpedo
        newanim.frameheight = 19
        newanim.xscale = newtorp.width / newanim.framewidth				-- don't change this
        newanim.yscale = newtorp.length / newanim.frameheight			-- don't change this
        newanim.yoffset = 10
        newanim.linkobj = newtorp                           -- this is an actual object. Use this to move the animation with this object
        newanim.speed = 0.2       -- seconds per frame
        newanim.rotation = newtorp.physobj.body:getAngle()         -- rotation is in rads
        newanim.currentframe = newanim.startframe
        newanim.timeleft = newanim.speed                -- how much time left on this frame
        newanim.kill = false
        table.insert(ANIMATIONS, newanim)
        newtorp.animation = newanim                     -- store the animation inside this object so it can be killed later on.
    else
        -- tube not loaded. Do nothing
    end
end

local function detectObjectsGeneric(obj, dt)
    -- each potential target has it's own timer in case they need to be checked on different cycles
    -- obj is a destroyer or a battleship (not submarine)

    obj.nextdetectioncheck = obj.nextdetectioncheck - dt
    if obj.nextdetectioncheck <= 0 then
        local subx, suby = SUBMARINE[1].physobj.body:getPosition()
        local objx, objy = obj.physobj.body:getPosition()
        local dist = cf.getDistance(subx, suby, objx, objy)
        fun.objectIsDetected(SUBMARINE[1], obj, dist, dt)           -- does rolls and sets obj values
        obj.nextdetectioncheck = NEXT_DETECTION_CHECK_MASTER
    end

    -- update timeundetected (if undetetect)
    if obj.isdetectedaudibly or obj.isdetectedvisually then
        obj.timeundetected = 0
    else
        obj.timeundetected = obj.timeundetected + dt    -- fun.drawobject will force a max value to this
    end
end

local function detectObjects(dt)
    for k, obj in pairs(DESTROYER) do
        detectObjectsGeneric(obj, dt)
    end

    for k, obj in pairs(BATTLESHIP) do
        detectObjectsGeneric(obj, dt)
    end
end

function submarine.draw(subobj)
    -- draws sub image
    fun.drawObject(subobj)
end

function submarine.update(dt)

    for k, sub in pairs(SUBMARINE) do
        physicslib.validateAngle(sub)          -- ensure angle is >= 0 and <= 359

        fun.applyHelm(sub, dt * TIME)
        fun.applyDepthChange(sub, sub.desireddepth, dt * TIME)
        fun.move(sub, dt)
        submarine.loadtubes(dt * TIME)

        detectObjects(dt)

        -- local heading = SUBMARINE[1].physobj.body:getAngle()
        -- print(heading)

    end

end

return submarine
