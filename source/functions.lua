functions = {}

function functions.loadImages()
    IMAGE[enum.imageSub] = love.graphics.newImage("assets/images/submarine.png")
    IMAGE[enum.imageTorpedo] = love.graphics.newImage("assets/images/torpedov2.png")
    IMAGE[enum.imageDestroyer] = love.graphics.newImage("assets/images/destroyer.png")
    IMAGE[enum.imageBattleship] = love.graphics.newImage("assets/images/ShipBattleshipHull.png")

    -- torp tube lights
    IMAGE[enum.imageLightOff] = love.graphics.newImage("assets/images/light_off.png")
    IMAGE[enum.imageLightGreen] = love.graphics.newImage("assets/images/green_light.png")
    IMAGE[enum.imageLightBlue] = love.graphics.newImage("assets/images/blue_light.png")

    IMAGE[enum.imageButtonRedLarge] = love.graphics.newImage("assets/images/red_button.png")
    IMAGE[enum.imageButtonOff] = love.graphics.newImage("assets/images/button_off.png")

    -- time acceleration buttons
    IMAGE[enum.imageButtonSpeedUp] = love.graphics.newImage("assets/images/button_plus.png")
    IMAGE[enum.imageButtonSpeedDown] = love.graphics.newImage("assets/images/button_minus.png")

    -- spritesheets
    IMAGE[enum.imageSheetSmokeFire] = love.graphics.newImage("assets/images/SmokeFireQuads.png")
    FRAMES[enum.imageSheetSmokeFire] = cf.fromImageToQuads(IMAGE[enum.imageSheetSmokeFire], 16, 16)
    FRAMES[enum.imageSheetSmokeFire].width = 16
    FRAMES[enum.imageSheetSmokeFire].height = 16

    IMAGE[enum.imageSheetDestroyerSinking] = love.graphics.newImage("assets/images/destroyersinkingsheet.png")
    FRAMES[enum.imageSheetDestroyerSinking] = cf.fromImageToQuads(IMAGE[enum.imageSheetDestroyerSinking], 20, 101)
    FRAMES[enum.imageSheetDestroyerSinking].width = 20
    FRAMES[enum.imageSheetDestroyerSinking].height = 101

    IMAGE[enum.imageSheetTorpedo] = love.graphics.newImage("assets/images/torpedo_sheet.png")
    FRAMES[enum.imageSheetTorpedo] = cf.fromImageToQuads(IMAGE[enum.imageSheetTorpedo], 14, 40)      -- spritewidth, spriteheight
    FRAMES[enum.imageSheetTorpedo].width = 14
    FRAMES[enum.imageSheetTorpedo].height = 40

    IMAGE[enum.imageSheetBattleshipSinking] = love.graphics.newImage("assets/images/battleshipsinkingsheet.png")
    FRAMES[enum.imageSheetBattleshipSinking] = cf.fromImageToQuads(IMAGE[enum.imageSheetBattleshipSinking], 32, 209)
    FRAMES[enum.imageSheetBattleshipSinking].width = 32
    FRAMES[enum.imageSheetBattleshipSinking].height = 209


    for i = 1, #IMAGE do
        if IMAGE[i] ~= nil then IMAGE[i]:setFilter("linear", "nearest") end     -- necessary due to enum.image not being sequential
    end
end

function functions.loadFonts()
    FONT[enum.fontDefault] = love.graphics.newFont("assets/fonts/Vera.ttf", 12)
    FONT[enum.fontMedium] = love.graphics.newFont("assets/fonts/Vera.ttf", 14)
    FONT[enum.fontLarge] = love.graphics.newFont("assets/fonts/Vera.ttf", 18)
    FONT[enum.fontCorporate] = love.graphics.newFont("assets/fonts/CorporateGothicNbpRegular-YJJ2.ttf", 36)
    FONT[enum.fontalienEncounters48] = love.graphics.newFont("assets/fonts/aliee13.ttf", 48)

    love.graphics.setFont(FONT[enum.fontDefault])
end

function functions.loadAudio()
    -- AUDIO[enum.audioMainMenu] = love.audio.newSource("assets/audio/XXX.mp3", "stream")           --"stream" or "static"
    AUDIO[enum.audioTorpedoLaunch] = love.audio.newSource("assets/audio/35530__jobro__torpedo-launch-underwater.wav", "static")
    AUDIO[enum.audioShipDestroyed1] = love.audio.newSource("assets/audio/ship_destroyedv2.ogg", "static")
    AUDIO[enum.audioMouseClick] = love.audio.newSource("assets/audio/click.wav", "static")
end

function functions.initialiseInputFields(sub)
    local physobj = sub.physobj
    local currentfacing = cf.round(math.deg(physobj.body:getAngle()))
    currentfacing = cf.adjustHeading(currentfacing, 90)

    -- remember to place an invisible button over the top of the inputfield
    INPUTFIELDS[enum.inputfieldDesiredFacing] = InputField(currentfacing)
	INPUTFIELDS[enum.inputfieldDesiredFacing].drawx = 405
	INPUTFIELDS[enum.inputfieldDesiredFacing].drawy = 50
	INPUTFIELDS[enum.inputfieldDesiredFacing].label = "Desired facing:"
	INPUTFIELDS[enum.inputfieldDesiredFacing]:setCursor(1)

    local drawy = 20
    local rowheight = 15
    local enumvalue = 2
    for i = 1, 5 do
        INPUTFIELDS[enumvalue] = InputField(currentfacing)
        INPUTFIELDS[enumvalue].drawx = 1050
        INPUTFIELDS[enumvalue].drawy = drawy
        INPUTFIELDS[enumvalue].label = "Tube #" .. i .. ": "
        INPUTFIELDS[enumvalue]:setCursor(1)
        drawy = drawy + rowheight
        enumvalue = enumvalue + 1
    end

    -- desired depth
    INPUTFIELDS[enum.inputfieldDesiredDepth] = InputField(SUBMARINE[1].depth)
	INPUTFIELDS[enum.inputfieldDesiredDepth].drawx = 550
	INPUTFIELDS[enum.inputfieldDesiredDepth].drawy = 50
	INPUTFIELDS[enum.inputfieldDesiredDepth].label = "Desired depth:"
	INPUTFIELDS[enum.inputfieldDesiredDepth]:setCursor(1)
end

function functions.applyHelm(obj, dt)
    -- apply steering forces if desired facing ~= current facing
    local physobj = obj.physobj

    local velx, vely = physobj.body:getLinearVelocity()

    if obj.speed > 0.5 then          -- check there is motion (this is different to engine setting)

        local currentfacing = math.deg(physobj.body:getAngle())         -- degrees
        currentfacing = cf.adjustHeading(currentfacing, 90)       -- converts this so zero = east
        local desiredfacing = tonumber(obj.desiredfacing)                         -- degrees

        if DEBUG and obj.objType == enum.objTypeSubmarine then
            -- print(obj.objType, desiredfacing, currentfacing, desiredfacing - currentfacing)
        end

        if desiredfacing == nil or desiredfacing < 0 or desiredfacing > 359 then        -- ensure desiredfacing is not nonsense
            -- do nothing
        else
            if math.abs(desiredfacing) - math.abs(currentfacing) >= -0.5 and math.abs(desiredfacing) - math.abs(currentfacing) <= 0.5 then
                -- do nothing due to facing being within tolerance
            else
                physicslib.turnToFacing(obj, dt)
                if DEBUG and obj.objType == enum.objTypeSubmarine then
                end
            end
        end
    end
end

function functions.applyDepthChange(obj, desireddepth, dt)
    -- submarine/depth charge depth change
    if obj.objType == enum.objTypeSubmarine then
        local delta = desireddepth - obj.depth
        local depthadj = delta * DEPTH_CHANGE * dt
        obj.depth = obj.depth + depthadj
        if obj.depth < 0 then obj.depth = 0 end
        if obj.depth > 250 then obj.depth = 250 end

        if obj.depth < 5 then
            obj.maxforce = obj.maxsurfaceforce
        else
            obj.maxforce = obj.maxunderwaterforce
        end
        SUBMARINE[1].currentforce = SUBMARINE[1].maxforce * SUBMARINE[1].enginesetting
    end
end

function functions.destroy(obj)
    -- destroy the provided object
    for i = #SUBMARINE, 1, -1 do
        if obj == SUBMARINE[i] then
            obj.physobj.body:destroy()
            table.remove(SUBMARINE, i)
            print("killed submarine")
            return
        end
    end
    for i = #TORPEDO, 1, -1 do
        if obj == TORPEDO[i] then
            if obj.animation ~= nil then obj.animation.kill = true end
            obj.physobj.body:destroy()
            table.remove(TORPEDO, i)
            print("killed torp")
            return
        end
    end
    for i = #DESTROYER, 1, -1 do
        if obj == DESTROYER[i] then
            if obj.animation ~= nil then obj.animation.kill = true end
            obj.physobj.body:destroy()
            table.remove(DESTROYER, i)
            print("killed destroyer")
            return
        end
    end
    for i = #BATTLESHIP, 1, -1 do
        if obj == BATTLESHIP[i] then
            if obj.animation ~= nil then obj.animation.kill = true end
            obj.physobj.body:destroy()
            table.remove(BATTLESHIP, i)
            print("killed battleship")
            return
        end
    end
end

function functions.detectObjects(obj, dt)
    -- obj = destroyer or battleship object

    if obj.alertlevel == enum.alertNormal then          -- do detect if already alert
        -- check if object detection range intersects with a torpedo
        for k, tp in pairs(TORPEDO) do
            local distance = cf.getDistance(obj.x, obj.y, tp.x, tp.y)       -- distance between obj and torpedo
            local area = cf.areaOfIntersectingCircles(obj.subdetectradius, tp.noiseradius, distance)
            local detectchance = (area / 8500 * 100) * dt            -- arbitrary value that seems to work. The dt accounts for the constant rnd roll on every loop
            detectchance = cf.round(detectchance)

            if area > 0 then print("area = " .. area) end
            if detectchance > 0 then print("detect chance = " .. detectchance) end

            if love.math.random(1, 100) < detectchance then
                -- print("Alert!! Detect chance: " .. detectchance)
                obj.alertlevel = enum.alertSearching
                obj.alerttimer = 60 * 5                      -- 5 minutes
                obj.currentforce = obj.maxforce
                obj.desiredfacing = cf.getBearing(obj.x, obj.y, tp.spawnx, tp.spawny)
                print("Destroyer moving to heading " .. obj.desiredfacing)
            end
        end
    else
        obj.alerttimer = obj.alerttimer - dt
        if obj.alerttimer <= 0 then
            obj.alerttimer = 0
            obj.alertlevel = enum.alertNormal
            obj.desiredfacing = obj.origfacing
            obj.currentforce = obj.origforce
        end
    end
end

function functions.adjustStopWatch(num, buttonID)
    -- num = the number of the stop watch
    -- advance the mode of the provided stopwatch
    assert (num > 0)
    assert(num <= #STOPWATCH)
    local enumvalue = buttonID

    if STOPWATCH[num].mode == enum.stopwatchReset then
        -- move to "started" mode
        STOPWATCH[num].mode = enum.stopwatchStarted
        buttons.changeButtonLabel(enumvalue, "Stop")
    elseif STOPWATCH[num].mode == enum.stopwatchStarted then
        -- move to "stop" mode
        STOPWATCH[num].mode = enum.stopwatchStopped
        buttons.changeButtonLabel(enumvalue, "Reset")
    elseif STOPWATCH[num].mode == enum.stopwatchStopped then
        -- move to "reset"
        STOPWATCH[num].mode = enum.stopwatchReset
        STOPWATCH[num].value = 0
        buttons.changeButtonLabel(enumvalue, "Start")
    else
        error()
    end
end

function functions.drawObject(obj)
    -- determine scaling by comparing image size with submarine size
    -- this is in addition to global scaling

    if obj.objType == enum.objTypeSubmarine or obj.objType == enum.objTypeTorpedo or obj.isdetectedvisually then
    -- if true then

        local objx, objy = obj.physobj.body:getPosition()

        local alpha         -- fades out the objects if fog affects them
        if obj.imageenum == nil then
            -- no image assigned. Do nothing
        else
            -- determine offset based on the image file
            local xoffset = IMAGE[obj.imageenum]:getWidth() / 2
            local yoffset = IMAGE[obj.imageenum]:getHeight() / 2

            local xscale = obj.width / IMAGE[obj.imageenum]:getWidth()
            local yscale = obj.length / IMAGE[obj.imageenum]:getHeight()


            if obj.timeundetected == nil then       -- not all things have timeundetected value
                alpha = 1
            else
                if obj.timeundetected > NEXT_DETECTION_CHECK_MASTER * 2 then obj.timeundetected = NEXT_DETECTION_CHECK_MASTER * 2 end
                alpha = obj.timeundetected / (NEXT_DETECTION_CHECK_MASTER * 2)              -- make it fade a bit slower
                alpha = 1 - alpha
            end

            -- draw the image
            love.graphics.setColor(1,1,1,alpha)
            local facingrad = math.rad(physicslib.getCompassHeading(obj))
            love.graphics.draw(IMAGE[obj.imageenum], objx, objy, facingrad, xscale, yscale, xoffset, yoffset)
        end

        -- draw noise profile
        if obj.objType == enum.objTypeSubmarine or (obj.objType == enum.objTypeTorpedo and DEBUG) then
            love.graphics.setColor(188/255, 0, 188/255, alpha)
            love.graphics.circle("line", objx, objy, obj.noiseradius)
        end

        -- draw detection radius
        if obj.subdetectradius ~= nil then
            love.graphics.setColor(1, 0, 0, alpha)
            love.graphics.circle("line", objx, objy, obj.subdetectradius)
        end

        if DEBUG then
            love.graphics.setColor(1,1,1,1)
            love.graphics.print("DH: " .. cf.round(obj.desiredfacing,1), objx + 15, objy + 5)
            if obj.isdetectedvisually then
                love.graphics.print("Detecting visually", objx + 15, objy + 20)
            end
            if obj.isdetectedaudibly then
                love.graphics.print("Detecting audibly", objx + 15, objy + 35)
            end
        end
    end
end

function functions.move(obj, dt)
    -- do NOT apply time here as it is applied in WORLD:update(dt * TIME)
    local physobj = obj.physobj

    local currentangle = physobj.body:getAngle()
        if obj.currentforce > obj.maxforce then obj.currentforce = obj.maxforce end
    if obj.currentforce < 0 then obj.currentforce = 0 end

    physobj.body:applyForce(math.cos(currentangle) * obj.currentforce, math.sin(currentangle) * obj.currentforce)

    local x, y = physobj.body:getLinearVelocity()
    obj.speed =  cf.getDistance(0,0,x,y)
    -- print("Speed: " .. speed)
end

function functions.addFlameAnimation(x, y, facingrad)
    -- flame animation
    local newanim = {}
    newanim.imageenum = enum.imageSheetSmokeFire        -- only the enum
    newanim.startframe = 9
    newanim.stopframe = 16
    newanim.linkobj = nil
    newanim.x = x
    newanim.y = y
    newanim.speed = 0.25       -- seconds per frame
    newanim.rotation = facingrad        -- radians
    newanim.currentframe = newanim.startframe
    newanim.timeleft = newanim.speed                -- how much time left on this frame
    newanim.kill = false
    table.insert(ANIMATIONS, newanim)
end

function functions.animationsdraw()
    love.graphics.setColor(1,1,1,1)
    for k, anim in pairs(ANIMATIONS) do
        -- calculate the offset
        local xoffset, yoffset
        if anim.xoffset == nil then
            xoffset = FRAMES[anim.imageenum].width / 2
        else
            xoffset = anim.xoffset
        end
        if anim.yoffset == nil then
            yoffset = FRAMES[anim.imageenum].height / 2
        else
            yoffset = anim.yoffset
        end
        love.graphics.draw(IMAGE[anim.imageenum], FRAMES[anim.imageenum][anim.currentframe], anim.x, anim.y, anim.rotation, anim.xscale, anim.yscale, xoffset, yoffset)
    end
end

function functions.animationsupdate(dt)
    -- update all animations

    for i = #ANIMATIONS, 1, -1 do
        if ANIMATIONS[i].kill == true then
            table.remove(ANIMATIONS, i)
        else
            -- update the position of the animation if it is attached to a moving object
            if ANIMATIONS[i].linkobj ~= nil then
                obj = ANIMATIONS[i].linkobj
                if obj.physobj ~= nil then
                    local objfacing = obj.physobj.body:getAngle()           -- rads
                    local x, y = obj.physobj.body:getPosition()
                    ANIMATIONS[i].x = x
                    ANIMATIONS[i].y = y
                    ANIMATIONS[i].rotation = objfacing + math.pi / 2          -- rads. 90 deg = pi / 2
                end
            end

            ANIMATIONS[i].timeleft = ANIMATIONS[i].timeleft - dt
            if ANIMATIONS[i].timeleft <= 0 then      -- advance to the next frame
                ANIMATIONS[i].currentframe = ANIMATIONS[i].currentframe + 1
                ANIMATIONS[i].timeleft = ANIMATIONS[i].speed
                if ANIMATIONS[i].currentframe > ANIMATIONS[i].stopframe then
                    if ANIMATIONS[i].endless then
                        ANIMATIONS[i].currentframe = ANIMATIONS[i].startframe
                    else
                        table.remove(ANIMATIONS, i)
                    end
                end
            end
        end
    end
end

function functions.increaseTime()
    cf.playAudio(enum.audioMouseClick, false, true)
    TIME = TIME + 1
    if TIME > 4 then TIME = 4 end
end

function functions.decreaseTime()
    cf.playAudio(enum.audioMouseClick, false, true)
    TIME = TIME - 1
    if TIME < 1 then TIME = 1 end
end

function functions.getCompassValue(degrees)
    -- converts "zero = east" value to "zero is north" value
    return cf.adjustHeading(degrees, 90)
end

function functions.assignCollider(obj)
    -- creates a collider and assigns it to the object. Returns nothing
    -- obj = an object

    -- collider. wind in counter-clockwise
    local x4, y4 = obj.x + (obj.width / 2), obj.y + (obj.length / 2)
    local x3, y3 = obj.x + (obj.width / 2), obj.y - (obj.length / 2)
    local x2, y2 = obj.x - (obj.width / 2), obj.y - (obj.length / 2)
    local x1, y1 = obj.x - (obj.width / 2), obj.y + (obj.length / 2)
    obj.collider = S.trikers.ConvexPolygon(x1,y1, x2,y2, x3,y3, x4,y4)
end

function functions.getObject(guid)
    -- cycle through OBJECTS until found GUID
    -- returns that object or nil
    for i = 1, #SUBMARINE do
        if SUBMARINE[i].guid == guid then
            return SUBMARINE[i]
        end
    end
    for i = 1, #TORPEDO do
        if TORPEDO[i].guid == guid then
            return TORPEDO[i]
        end
    end
    for i = 1, #BATTLESHIP do
        if BATTLESHIP[i].guid == guid then
            return BATTLESHIP[i]
        end
    end
    for i = 1, #DESTROYER do
        if DESTROYER[i].guid == guid then
            return DESTROYER[i]
        end
    end
    return nil
end

function functions.updateWorldClock(dt)
    GAME_SECOND = GAME_SECOND + (TIME * dt)
    if GAME_SECOND > 59 then
        GAME_SECOND = 0
        GAME_MINUTE = GAME_MINUTE + 1
        if GAME_MINUTE > 60 then
            GAME_MINUTE = 0
            GAME_HOUR = GAME_HOUR + 1
            if GAME_HOUR > 23 then
                GAME_HOUR = 0
                GAME_DATE = GAME_DATE + 1
            end
        end
    end
end

function functions.objectIsDetected(observer, observee, distance, dt)
    -- see if an observer, that is 'distance' away from 'observee' can detect that 'observee'
    -- observer is the object looking. Observee is the object the observer is trying to see
    -- factors visual and audio as two difference "dice rolls"
    -- dt is used due to the constant checks per game loop

    -- set here then override later
    observee.isdetectedvisually = false
    observee.isdetectedaudibly = false

    local visualradius = observee.visualradius

    if observer.depth > 15 then visualradius = 0 end        -- can't see anything if not at periscope depth (or surface)

    -- check for visual detection
    if distance > visualradius then
        observee.isdetectedvisually = false
        -- print("Target outside visual range. No chance of detecting it")
    else
        local percent = (distance / visualradius) * 100 * dt            -- this is likelihood to NOT be detected. Decreases as distance shrinks
        if love.math.random(1, 100) > percent then
            observee.isdetectedvisually = true
            -- print("Detected object with " .. distance .. "metres and " .. percent .. " chance")
        else
            -- print("Failed to detect object with " .. distance .. "metres and " .. percent .. " chance")
            observee.isdetectedvisually = false
        end
    end

    -- check for audio detection
    if distance > observee.noiseradius then
        observee.isdetectedaudibly = false
    else
        local percent = (distance / observee.noiseradius) * 100 * dt            -- this is likelihood to NOT be detected. Decreases as distance shrinks
        if love.math.random(1, 100) > percent then
            observee.isdetectedaudibly = true
        end
    end

    -- if detectvisual and detectaudio then                --! check that the NOT works correctly
    --     return enum.detectedVisualAudio
    -- elseif detectvisual and not detectaudio then
    --     return enum.detectedVisual
    -- elseif not detectvisual and detectaudio then
    --     return enum.detectedAudio
    -- elseif not detectvisual and not detectaudio then
    --     return enum.detectedNone
    -- else
    --     error()
    -- end
end


return functions
