torp = {}

function torp.create(sub, tube)
    -- create a torp from the nominated tube
    -- sub: the sub object
    -- tube: the tube number (digit)
    -- returns a torpedo object

    -- https://en.wikipedia.org/wiki/G7e_torpedo  (G7e(TIII)
    -- range: 7,500 m  (500 seconds @ 15 m/s)
    -- speed: 56 km/h (30 kn) (15 m/s)

    -- https://maritime.org/doc/torpedo/
    -- https://www.ibiblio.org/hyperwar/USN/Admin-Hist/BuOrd/BuOrd-6.html

    local newtorp = {}
    newtorp.objType = enum.objTypeTorpedo

    newtorp.desiredfacing = INPUTFIELDS[1 + tube]:getText()      -- this is a one time text capture. Also a fudge using enums

    -- determine the x/y which is the front of the sub
    newtorp.length = 7
    newtorp.width = 0.533
    newtorp.depth = 10
    newtorp.desireddepth = newtorp.depth
    -- if DEBUG then
        -- newtorp.timeleft = 30      -- seconds to run
    -- else
        newtorp.timeleft = TORPEDO_RUN_TIME      -- seconds to run
    -- end
    newtorp.mass = 280                          -- https://en.wikipedia.org/wiki/List_of_torpedoes_by_name
    newtorp.speed = 0                           -- straight-line speed in m/s based on x/y velocity
    newtorp.maxforce = 27                     -- speed: 56 km/h (30 kn) (15 m/s)
    newtorp.currentforce = newtorp.maxforce
    newtorp.turnrate = 10
    newtorp.noiseradius = 100
    newtorp.visualradius = 15
    -- newtorp.imageenum = newtorp.objType  -- torpedo uses animation

    -- create a physical object and attach it to the table
    local thisobject = {}
    local subx, suby = sub.physobj.body:getPosition()
    local subfacing = physicslib.getCompassHeading(sub)     -- compass degrees

    -- print("Sub facing value is " .. subfacing .. " compass degrees")

    local halfsub = sub.length / 2
    local x, y = cf.addVectorToPoint(subx, suby, subfacing, halfsub * 1.5)          -- make sure torp does not spawn on top of sub
    thisobject.body = love.physics.newBody(PHYSICSWORLD, x, y, "dynamic")
    thisobject.body:setLinearDamping(0.5)
    thisobject.body:setMass(newtorp.mass)
    thisobject.body:setAngle(sub.physobj.body:getAngle())           -- just apply the unconverted radian

    -- define the shape of the physical object
    local x1 = newtorp.length / 2 * -1
    local y1 = newtorp.width / 2 * -1
    local x2 = newtorp.length / 2
    local y2 = newtorp.width / 2 * -1
    local x3 = newtorp.length / 2
    local y3 = newtorp.width / 2
    local x4 = newtorp.length / 2 * -1
    local y4 = newtorp.width / 2
    thisobject.shape = love.physics.newPolygonShape(x2,y2,x3,y3,x4,y4,x1,y1)
    thisobject.fixture = love.physics.newFixture(thisobject.body, thisobject.shape, 1)		-- the 1 is the density
    thisobject.fixture:setRestitution(0.25)                                                 -- bounce factor
    thisobject.fixture:setSensor(false)                                                     -- sensors don't react physically so set this to false
    thisobject.fixture:setCategory(enum.objTypeTorpedo)
    thisobject.fixture:setMask(enum.objTypeTorpedo)     -- this is what it WON'T collide with

    local guid = cf.getGUID()
    thisobject.fixture:setUserData(guid)
    newtorp.guid = guid

    newtorp.physobj = thisobject

    newtorp.spawnx = x      -- captures where the torp was fired so enemy can track  back to this point
    newtorp.spawny = y

    -- newtorp.imageenum = enum.imageTorpedo
    newtorp.imageenum = nil                             -- uses an animation on creattion

    table.insert(TORPEDO, newtorp)
    return newtorp
end

function torp.remove(tp)

    for i = #TORPEDO, 0, -1 do
        if TORPEDO[i] == tp then
            TORPEDO[i].physobj.body:destroy()
            table.remove(TORPEDO, i)
            return
        end
    end
end

function torp.draw(torp)
    -- torp = object

    local torpx, torpy = torp.physobj.body:getPosition()

    fun.drawObject(torp)

    -- draw time to run


    -- draw heading and speed
    if DEBUG then
        love.graphics.setColor(1,1,1,1)
        love.graphics.print(cf.round(torp.timeleft, 0), (torpx + 5), (torpy - 5))
        love.graphics.print(cf.round(torp.speed, 0) .. " m/s", torpx + 5, torpy + 15)
    end
end

function torp.update(dt)
    for k, tp in pairs(TORPEDO) do
        physicslib.validateAngle(tp)          -- ensure angle is >= 0 and <= 359

        fun.applyHelm(tp, dt)
        fun.move(tp, dt)

        tp.timeleft = tp.timeleft - dt
        if tp.timeleft <= 0 then
            tp.animation.kill = true                -- kill the animation before killing the object
            torp.remove(tp)
        end
    end
end

return torp
